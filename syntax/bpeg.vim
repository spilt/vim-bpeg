" Language:    BPEG
" Maintainer:  Bruce Hill <bruce@bruce-hill.com>
" License:     WTFPL

" Bail if our syntax is already loaded.
if exists('b:current_syntax') && b:current_syntax == 'bpeg'
  finish
endif

syn region BPEGBlockComment start=/#(/ end=/)#/ contains=BPEGBlockComment
hi def link BPEGBlockComment Comment
hi BPEGBlockComment ctermfg=DarkBlue

syn region BPEGComment start=/#(\@!/ end=/$/
hi def link BPEGComment Comment
hi BPEGComment ctermfg=DarkBlue

syn region BPEGString start=/\z(["']\)/ end=/\z1/ contains=BPEGEscape
hi def link BPEGString String

syn region BPEGWord start=/{/ end=/}/
hi def link BPEGWord String

syn match BPEGChars /`.\(-.\)\?\(,.\(-.\)\?\)*/
hi def link BPEGChars String

syn match BPEGEscape /\\\([abenrtvN]\|x\x\x\|\d\{3}\)\(-\([abnrtv]\|x\x\x\|\d\{3}\)\)\?/
hi BPEGEscape ctermfg=LightBlue

syn match BPEGOperator ;=>\|\~\|!\~\|[<>!%^$.];
hi BPEGOperator ctermfg=White

syn match BPEGCapture /@\(\([a-zA-Z][a-zA-Z0-9-]*\)=\)\?/
hi BPEGCapture ctermfg=Blue

syn match BPEGSlash ;/;
hi BPEGSlash ctermfg=DarkGray

syn match BPEGNumber /\d\+\(\s*+\|\s*-\s*\d*\)\?/
hi BPEGNumber ctermfg=Cyan

syn match BPEGParens /[()]/
hi BPEGParens ctermfg=Yellow

syn match BPEGError /(!)/
hi BPEGError ctermfg=red

syn match BPEGOptional /\[\|\]/
hi BPEGOptional ctermfg=Gray

syn match BPEGSpace /_\+/
hi BPEGSpace ctermfg=Gray

syn match BPEGName /[a-zA-Z][a-zA-Z0-9-]*/
hi BPEGName ctermfg=none

syn match BPEGDef /[a-zA-Z][a-zA-Z0-9-]*\s*:/ contains=BPEGDefName
hi BPEGDef ctermfg=White cterm=bold

syn cluster BPEGAll contains=BPEGBlockComment,BPEGComment,BPEGWord,BPEGString,BPEGOperator,BPEGSlash,BPEGChars,
      \BPEGEscape,BPEGCapture,BPEGError,BPEGParens,BPEGOptional,BPEGDef,BPEGNumber,BPEGSpace

if !exists('b:current_syntax')
  let b:current_syntax = 'bpeg'
endif
