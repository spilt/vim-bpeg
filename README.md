This is a vim plugin for a (currently work in progress) Better Parsing Expression Grammars.
I recommend installing it with [vim-plug](https://github.com/junegunn/vim-plug):

```
Plug 'https://bitbucket.org/spilt/vim-bpeg'
```
