" Language:    BPEG
" Maintainer:  Bruce Hill <bruce@bruce-hill.com>
" License:     WTFPL

autocmd BufNewFile,BufRead *.bpeg set filetype=bpeg
autocmd BufNewFile,BufRead *.bp set filetype=bpeg
